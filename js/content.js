window.addEventListener('scroll', e => {
    chrome.storage.local.set({'scroll': {host: location.host, pathname: location.pathname, scrollY: window.scrollY}})
})

let itvSroll = undefined;
chrome.storage.local.onChanged.addListener(object => {
    let scroll = object.scroll
    if (location.host !== scroll.host && location.pathname === scroll.newValue.pathname) {
        clearTimeout(itvSroll)
        itvSroll = setTimeout(() => {
            window.scrollTo(0, scroll.newValue.scrollY);     
        }, 50)
    }
})